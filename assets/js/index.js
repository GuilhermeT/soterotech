// Função para salvar os dados preenchidos nos campos
function save() {
	first_name = document.getElementById('first').value;
	last_name  = document.getElementById('last').value;
	part = document.getElementById('part').value;
	
	if (( first_name.replace(/\s*/g, '').length > 0 != false ) && ( last_name.replace(/\s*/g, '').length > 0 != false ) && ( part.replace(/\s*/g, '').length > 0 != false )) {
		var ajax = new XMLHttpRequest();

		ajax.open("GET", "http://localhost/soterotech/api/insert.php/?first="+first_name+"&last="+last_name+"&part="+part, true);

		ajax.send();

		ajax.onreadystatechange = function() {

			if (ajax.readyState == 4 && ajax.status == 200) {
				window.location.reload();
			}
		}
	} else {
		document.getElementById('alert').style.display="block";
	}
}

// Requisição ajax
var ajax = new XMLHttpRequest();

ajax.open("GET", "http://localhost/soterotech/api/select.php", true);

ajax.send();

// Função ajax com a resposta  da requisição
ajax.onreadystatechange = function() {
	if (ajax.readyState == 4 && ajax.status == 200) {
		let id    = [];
		let names = [];
		let participation = [];
		let total = 0;
		let porcentagem = []

		data = JSON.parse(ajax.responseText);
		
		// FORs para preencher os arrays
		for (let i = 0; i < data.length; i++) {
			id[i] = data[i]["id"];
		}

		for (let i = 0; i < data.length; i++) {
			names[i] = data[i]["first_name"]+" "+data[i]["last_name"];
		}

		for (let i = 0; i < data.length; i++) {
			total += parseInt(data[i]["participation"]);
			participation[i] = data[i]["participation"];
		}

		// FOR para fazer o calculo das porcentagens das participações
		for (let i = 0; i < participation.length; i++) {
			porcentagem[i] = (participation[i] * 100) / total; 
			porcentagem[i] = parseFloat(porcentagem[i].toFixed(2));
		}
		
		// Monta a tabela com seus valores
		table = document.getElementById('bodytable');

		for (let i = 0; i < data.length; i++) {
			tr  = document.createElement('tr');
			th  = document.createElement('th');
			td1 = document.createElement('td');
			td2 = document.createElement('td');
			td3 = document.createElement('td');

			let name = names[i].split(' ');

			let thText  = document.createTextNode(id[i]);
			let td1Text = document.createTextNode(name[0]);
			let td2Text = document.createTextNode(name[1]);
			let td3Text = document.createTextNode(participation[i]);

			th.appendChild(thText);
			td1.appendChild(td1Text);
			td2.appendChild(td2Text);
			td3.appendChild(td3Text);

			tr.appendChild(th);
			tr.appendChild(td1);
			tr.appendChild(td2);
			tr.appendChild(td3);

			table.appendChild(tr);
		}

		// Monta o gráfico com suas configurações e valores
		var ctx = document.getElementById('myChart').getContext('2d');
		var myChart = new Chart(ctx, {
		    type: 'doughnut',
		    data: {
		        labels: names,
		        datasets: [{
		            label: 'Data',
		            data: porcentagem,
		            backgroundColor: [
		                'rgba(255,  99, 132, 1)',
		                'rgba( 54, 162, 235, 1)',
		                'rgba(255, 206,  86, 1)',
		                'rgba( 75, 192, 192, 1)',
		                'rgba(153, 102, 255, 1)',
		                'rgba(255, 159,  64, 1)'
		            ],
		            borderColor: [
		                'rgba(255, 99, 132, 1)',
		                'rgba(54, 162, 235, 1)',
		                'rgba(255, 206, 86, 1)',
		                'rgba(75, 192, 192, 1)',
		                'rgba(153, 102, 255, 1)',
		                'rgba(255, 159, 64, 1)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});
	}
}