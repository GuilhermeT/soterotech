<?php

class Crud
{
	// Função estática para inserir os dados no banco de dados
	static function insert($first, $last, $part)
	{
		try {
			// Conexão com banco e preparo da query
			$conn = new PDO('mysql:host=localhost;dbname=teste', 'root', '');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$stmt= $conn->prepare("INSERT INTO data (first_name, last_name, participation) VALUES (?,?,?)");

			// Inseri no banco
			$stmt->execute([$first, $last, $part]);
			die("OK");
		} catch (Exception $e) {
			die("Erro");
		}
	}

	// Função estática para pegar os dados no banco de dados
	static function select()
	{
		try {	
			// Conexão com banco e preparo da query
			// Alterar o usuário e senha
			$conn = new PDO('mysql:host=localhost;dbname=teste', 'root', '');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  		$data = $conn->query('SELECT * FROM data');

	  		// Inserir os dados no array e transforma em json
	  		$results = array();
	  		while ($linha = $data->fetch(PDO::FETCH_ASSOC)) {
    			$results[] = $linha;;
			}
	  		die(json_encode($results));
		} catch (Exception $e) {
			die("Erro");
		}
	}
}