<?php

// Importa o arquivo Crud
require('../model/Crud.php');

// Verifica quantos parâmetros foi passado na url, captura e passa para o método estático da classe Crud
if (count($_REQUEST) == 3) {
	try {		
		$first = strval($_REQUEST["first"]);
		$last  = strval($_REQUEST["last"]);
		$part  = strval($_REQUEST["part"]);
		if (!empty($first) && !empty($last) && !empty($part)) {
			Crud::insert($first, $last, $part);
		} else {
			die("Erro");
		}
	} catch (Exception $e) {
		die("Erro");
	}
} else {
	die("Erro");
}