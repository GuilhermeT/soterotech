# Soterotech

## Aquivo index.php

	Redireciona para o arquivo dashboard.html.

## Diretório API

	Possui 2 arquivos insert.php e select.php.
		insert.php tratam as requisições para cadastrar as informações.
		select.php retorna as informações cadastradas.

## Diretório assets

	Possui 2 pastas css e js.
		1. css
			Possui 2 arquivos bootstrap.min.css e index.css.
			bootstrap.min.css arquivo do framework bootstrap.
			index.css arquivo com designer principal do sistema.
		2. js
			Possui 3 arquivos bootstrap.min.js, chart.min.js e index.js.
			bootstrap.min.js arquivo do framework bootstrap.
			chart.min.js arquivo da biblioteca chartjs.
			index.js arquivo com validação dos formulários e lógica do gráfico principal dos sistema.

## Diretório dump

	Possui o arquivo do dump do banco de dados.

## Diretório model

	Possui 1 arquivo Crud.php.
	arquivo onde possui a conexão com banco e a manipulação.
	OBS: Precisa alterar o usuário e colocar senha caso for preciso.

## Diretório src

	Possui 1 arquivo dashboard.html.
	arquivo principal com gráfico e formulário.
